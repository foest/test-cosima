Welcome to cosima's documentation!
===================================

*cosima* is a tool to investigate the interactions between the power system and communciation network by integrating the (communication) simulator `OMNeT++ <https://omnetpp.org/>`_ into the co-simulation framework `mosaik <https://mosaik.offis.de/>`_
.
Check out the :doc:`Overview` section for further information, including
how to install the project.

.. note::

   This project is under active development.


.. toctree::
   :maxdepth: 1
   :caption: Contents:


   QuickStart
   Scenarios
   Synchronization
   Message Types
   CommunicationSimulator
   ICTController
   Networks
   MosaikScheduler
   Agent Apps

